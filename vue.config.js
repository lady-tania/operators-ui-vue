process.env.VUE_APP_VERSION = require('./package.json').version;
module.exports = {
  lintOnSave: false,

  devServer: {
    port: 8080,
  },
  transpileDependencies: [
    'vuetify',
  ],
};
