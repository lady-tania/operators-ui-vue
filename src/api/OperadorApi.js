import AxiosOperatorsApp from './AxiosOperatorsApp';

const pathOperadoresResource = '/api/operadores';

export default {
  getOperators() {
    return AxiosOperatorsApp.get(pathOperadoresResource);
  },
  add(operador) {
    return AxiosOperatorsApp.post(pathOperadoresResource, operador);
  },
  edit(operador) {
    return AxiosOperatorsApp.put(`${pathOperadoresResource}/${operador.id}`, operador);
  },
  logicalDelete(id) {
    return AxiosOperatorsApp.delete(`${pathOperadoresResource}/${id}`);
  },
};
