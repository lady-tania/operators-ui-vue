import axios from 'axios';

const AxiosOperatorsApp = axios.create({
  baseURL: process.env.VUE_APP_BASE_URL,
  headers: {
    // aqui definir headers personalizados
    // Authorization: `Bearer ${localStorage.getItem('user-token')}`
  },
});

// Add a request interceptor
AxiosOperatorsApp.interceptors.request.use(function (config) {
  // Do something before request is sent
  return config;
}, function (error) {
  // Do something with request error
  return Promise.reject(error);
});

// Add a response interceptor
AxiosOperatorsApp.interceptors.response.use(function (response) {
  // Any status code that lie within the range of 2xx cause this function to trigger
  // Do something with response data
  return response;
}, function (error) {
  // objects you can use to process error: error.response.data, error.response.status, error.response.headers
  // Do something with response error
  // console.log(error.response.data)
  this.$appSnackbarError = true;
  return Promise.reject(error);
});

export { AxiosOperatorsApp as default }
