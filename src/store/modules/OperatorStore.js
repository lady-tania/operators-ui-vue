import OperadorApi from '../../api/OperadorApi';

const mutations = {
  showOperatorDialogMut(state) {
    state.operatorDialog = true;
  },
  hideOperatorDialogMut(state) {
    state.operatorDialog = false;
  },
  setOperatorsMut(state, OperatorsFromAction) {
    state.operators = OperatorsFromAction;
  },
  addOperatorMut(state, newOperatorFromAction) {
    state.operators.push(newOperatorFromAction);
  },
  updateOperatorListMut(state, operatorEdited) {
    const index = state.operators.findIndex((obj) => obj.id === operatorEdited.id);
    state.operators.splice(index, 1, operatorEdited);
    state.operators = [...state.operators]; // replace operators
  },
  logicalDeleteMut(state, operatorFromAction) {
    state.operators = [...state.operators.filter((obj) => obj.id !== operatorFromAction.id)]; // replace operators
  },
};

const actions = {
  async getOperatorsAction({ commit }) {
    try {
      const response = await OperadorApi.getOperators();
      commit('setOperatorsMut', response.data);
    } catch (error) {
      commit('setOperatorsMut', []);
    }
  },
  async addOperatorAction({ commit }, operator) {
    let newOperator = null;
    try {
      const response = await OperadorApi.add(operator);
      newOperator = response.data;
      commit('addOperatorMut', newOperator);
      commit('showSuccessMessageMut', 'Se creó el operador '+ newOperator.nombres +' correctamente!', { root: true });
    } catch (e) {
      console.error(e);
    }
    return newOperator;
  },
  async editOperatorAction({ commit }, operator) {
    let operatorEdited = null;
    try {
      const response = await OperadorApi.edit(operator);
      operatorEdited = response.data;
      commit('updateOperatorListMut', operatorEdited);
      commit('showSuccessMessageMut', 'Se modificó el operador ' + operatorEdited.nombres + ' correctamente!', { root: true });
    } catch (e) {
      console.error(e);
    }
    return operatorEdited;
  },
  async logicalDeleteAction({ commit }, operator) {
    try {
      await OperadorApi.logicalDelete(operator.id);
      commit('logicalDeleteMut', operator);
      commit('showSuccessMessageMut', 'Se eliminó el operador ' + operator.nombres + ' correctamente!', { root: true });
    } catch (e) {
      console.log(e);
    }
  },
};

const state = {
  operatorDialog: false,
  operators: [],
};

/**
 * good totorial for getters
 * https://medium.com/js-dojo/vuex-2638ba4b1d76
 * https://elabismodenull.wordpress.com/2017/05/31/vuejs-los-estados-y-getters-en-vuex/
 */
const getters = {};

export default {
  namespaced: true,
  actions,
  mutations,
  state,
  getters,
};
