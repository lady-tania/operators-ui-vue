import Vue from 'vue';
import Vuex from 'vuex';
import createLogger from 'vuex/dist/logger';
import OperatorStore from './modules/OperatorStore';

Vue.use(Vuex);

const debug = process.env.NODE_ENV === 'development';

export default new Vuex.Store({
  state: {
    appSnackbarMessageArrayState: [],
  },
  mutations: {
    showSuccessMessageMut(state, message) {
      state.appSnackbarMessageArrayState.push(message);
    },
    shiftAppSnackbarSuccessMut(state) {
      state.appSnackbarMessageArrayState.shift();
    },
  },
  actions: {
  },
  modules: {
    OperatorStore,
  },
  strict: debug,
  plugins: debug ? [createLogger()] : [],
});
